import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import loading from "./loading"



export default new Vuex.Store({
	 plugins:[],
    modules:{
         loading,
    },
})